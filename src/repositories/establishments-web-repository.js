const mongoose = require("mongoose");
const wines = require("../models/wines");
const moment = require("moment");
const establishments = require("../models/establishments");
mongoose.model("user");
mongoose.model("Wines");
mongoose.model("Establishments");

exports.createEstablishments = async data => {
  const establishment = new establishments(data);
  await establishment.save();

  return establishment;
};

exports.updateEstablishments = async (id, data) => {
  await establishments.findByIdAndUpdate(id, {
    $set: data
  });
};

exports.deleteEstablishments = async id => {
  await establishments.findOneAndRemove(id);
};

exports.insertWineEstablishment = async (id, idWine, priceWine) => {
  var data_create = moment().subtract(1, "hours");
  var data_create_save = data_create.toString();
  console.log(data_create_save);
  await establishments.findByIdAndUpdate(id, {
    $push: {
      establishment_listWines: {
        establishment_idWine: idWine,
        establishment_wine_updated_time: "",
        establishment_wine_created_time: data_create_save,
        establishment_priceWine: priceWine,
        establishment_last_price: ""
      }
    }
  });
};

exports.createImagesEstablishments = async (id, data) => {
  image1 = data.establishment_image1;
  image2 = data.establishment_image2;
  image3 = data.establishment_image3;
  image4 = data.establishment_image4;
  array = [image1, image2, image3, image4];

  array.forEach(async element => {
    await establishments.findByIdAndUpdate(id, {
      $push: {
        establishment_images: element
      }
    });
  });
  return "";
};

exports.listwinesOfEstablishments = async id => {
  try {
    const establishment_wine_list_info = await establishments.findById(
      id,
      "establishment_listWines"
    );

    var jsonStr = [];
    var jsonFinal = [];
    jsonStr = JSON.parse(
      JSON.stringify(establishment_wine_list_info.establishment_listWines)
    );

    for (const [idx, wineObj] of jsonStr.entries()) {
      const wine_information_db = await wines.findById(
        wineObj.establishment_idWine,
        { wine_price: 0, imagesWines: 0 }
      );
      jsonFinal.push({ establishment_wine_list_info, wine_information_db });
    }
    jsonFinal2 = JSON.parse(JSON.stringify(jsonFinal));
    return jsonFinal2;
  } catch (err) {
    console.error(err);
  }
};

exports.findEstablishmentById = async establishment_id => {
  try {
    return await establishments.find({
      _id: establishment_id
    });
  } catch (error) {
    console.log(error);
  }
};

exports.setOrderHistoric = async (establishment_id, data) => {
  try {
    await establishments.findByIdAndUpdate(establishment_id, {
      $push: { establishment_orders_historic: data }
    });
  } catch (err) {
    console.error(err);
  }
};

exports.alterOrderStatusToCanceled = async (
  establishment_id,
  order_id_find,
  canceled_by,
  cancel_reason
) => {
  try {
    await establishments.updateOne(
      {
        _id: establishment_id,
        "establishment_orders_historic.order_id": order_id_find
      },
      {
        $set: {
          "establishment_orders_historic.$.order_status_payment": "refunded",
          "establishment_orders_historic.$.order_status_establishment":
            "canceled",
          "establishment_orders_historic.$.order_canceled_by": canceled_by,
          "establishment_orders_historic.$.order_canceled_reason": cancel_reason
        }
      }
    );
  } catch (err) {
    console.error(err);
  }
};

exports.updateWineOfEstablishment = async (
  establishment_id,
  wine_id,
  new_price
) => {
  try {
    var wine_updated = moment()
      .subtract(1, "hours")
      .toString();

    var wineLastPrice = await establishments.findById({
      _id: establishment_id,
      "establishment_listWines.establishment_idWine": wine_id
    });
    var last_price = "";
    var wineLastPriceFinal = JSON.parse(JSON.stringify(wineLastPrice));
    wineLastPriceFinal.establishment_listWines.forEach(element => {
      if (element.establishment_idWine == wine_id) {
        last_price = element.establishment_priceWine;
      }
    });
    await establishments.updateOne(
      {
        _id: establishment_id,
        "establishment_listWines.establishment_idWine": wine_id
      },
      {
        $set: {
          "establishment_listWines.$.establishment_priceWine": new_price,
          "establishment_listWines.$.establishment_wine_updated_time": wine_updated,
          "establishment_listWines.$.establishment_last_price": last_price
        }
      }
    );
  } catch (err) {
    console.error(err);
  }
};
