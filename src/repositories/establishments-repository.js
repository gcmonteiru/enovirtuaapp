const mongoose = require("mongoose");
const users = require("../models/user");
const wines = require("../models/wines");
const geolib = require("geolib");
const moment = require("moment");
const establishments = require("../models/establishments");
mongoose.model("user");
mongoose.model("Wines");
mongoose.model("Establishments");
var userid = "";

exports.listEstablishments = async (id, data, limit, skip) => {
  userid = id;
  try {
    await users.findByIdAndUpdate(id, {
      $set: data
    });

    const usersRes = await users.find({ _id: userid });

    var usersResObj = JSON.parse(JSON.stringify(usersRes));
    var xcoordinate = usersResObj[0].user_Xcoordinate;
    var ycoordinate = usersResObj[0].user_Ycoordinate;
    var kmdesired = usersResObj[0].user_desiredDistanceInKm;

    const establishmentsRes = await establishments
      .find({})
      .skip(Number(skip))
      .limit(Number(limit));

    var establishmentsResObj = JSON.parse(JSON.stringify(establishmentsRes));

    var distance = "";
    var count = 0;
    establishmentsResObj.forEach(element => {
      distance = geolib.convertDistance(
        geolib.getDistance(
          {
            latitude: element["establishment_Xcoordinate"],
            longitude: element["establishment_Ycoordinate"]
          },
          { latitude: xcoordinate, longitude: ycoordinate },
          1
        ),
        "km"
      );
      if (distance <= kmdesired) {
        establishmentsResObj[count].establishment_distanceFromUser = distance;
      } else {
        establishmentsResObj[count] = "";
      }

      count = count + 1;
    });

    var filtered = establishmentsResObj.filter(function(el) {
      return el != "";
    });

    filtered.sort(function(a, b) {
      return (
        a.establishment_distanceFromUser - b.establishment_distanceFromUser
      );
    });

    jsonStr = JSON.stringify(filtered);
    return jsonStr;
  } catch (err) {
    console.error(err);
  }
};

exports.searchEstablishments = async (search, userid, limit, skip) => {
  const usersRes = await users.find({ _id: userid });

  var usersResObj = JSON.parse(JSON.stringify(usersRes));
  var xcoordinate = usersResObj[0].user_Xcoordinate;
  var ycoordinate = usersResObj[0].user_Ycoordinate;
  var kmdesired = usersResObj[0].user_desiredDistanceInKm;
  var searchEstablishment = await establishments
    .find({
      establishment_name: { $regex: new RegExp(search), $options: "i" }
    })
    .skip(Number(skip))
    .limit(Number(limit));

  var establishmentsResObj = JSON.parse(JSON.stringify(searchEstablishment));
  var distance = "";
  var count = 0;
  establishmentsResObj.forEach(element => {
    distance = geolib.convertDistance(
      geolib.getDistance(
        {
          latitude: element["establishment_Xcoordinate"],
          longitude: element["establishment_Ycoordinate"]
        },
        { latitude: xcoordinate, longitude: ycoordinate },
        1
      ),
      "km"
    );
    if (distance <= kmdesired) {
      establishmentsResObj[count].establishment_distanceFromUser = distance;
    } else {
      establishmentsResObj[count] = "";
    }

    count = count + 1;
  });

  var filtered = establishmentsResObj.filter(function(el) {
    return el != "";
  });

  filtered.sort(function(a, b) {
    return a.establishment_distanceFromUser - b.establishment_distanceFromUser;
  });

  jsonStr = JSON.stringify(filtered);

  return jsonStr;
};

exports.listwinesOfEstablishments = async id => {
  try {
    const establishment_wine_list_info = await establishments.findById(id, {
      establishment_listWines: 1,
      _id: 0
    });
    var teste = [];

    var jsonStr = [];
    var jsonFinal = [];
    jsonStr = JSON.parse(
      JSON.stringify(establishment_wine_list_info.establishment_listWines)
    );

    for (const [idx, wineObj] of jsonStr.entries()) {
      const wine_information_db = await wines.findById(
        wineObj.establishment_idWine,
        { wine_price: 0, imagesWines: 0 }
      );

      wineObj["wine_country"] = wine_information_db.wine_country;
      wineObj["wine_name"] = wine_information_db.wine_name;
      wineObj["wine_region"] = wine_information_db.wine_region;
      wineObj["wine_winery"] = wine_information_db.wine_winery;
      wineObj["wine_harvest"] = wine_information_db.wine_harvest;
      wineObj["wine_grape"] = wine_information_db.wine_grape;
      wineObj["_id"] = wine_information_db._id;
      wineObj["wine_varietal"] = wine_information_db.wine_varietal;
      wineObj["wine_description"] = wine_information_db.wine_description;
      wineObj["wine_classification"] = wine_information_db.wine_classification;
      wineObj["wine_viticultura"] = wine_information_db.wine_viticultura;
      wineObj["wine_terroir"] = wine_information_db.wine_terroir;
      wineObj["wine_harmonization"] = wine_information_db.wine_harmonization;
      wineObj["wine_consumption"] = wine_information_db.wine_consumption;
      wineObj["wine_tenor"] = wine_information_db.wine_tenor;
      wineObj["wine_volume"] = wine_information_db.wine_volume;
      wineObj["wine_acidity"] = wine_information_db.wine_acidity;
      wineObj["wine_ph"] = wine_information_db.wine_ph;
      wineObj["wine_vinification"] = wine_information_db.wine_vinification;
      wineObj["wine_aroma"] = wine_information_db.wine_aroma;
      wineObj["wine_color"] = wine_information_db.wine_color;
      wineObj["wine_guarda"] = wine_information_db.wine_guarda;
      wineObj["wine_rewards"] = wine_information_db.wine_rewards;
      wineObj["wine_label"] = wine_information_db.wine_label;
      wineObj["wine_degustation"] = wine_information_db.wine_degustation;
      wineObj["wine_register"] = wine_information_db.wine_register;
      wineObj["wine_autor"] = wine_information_db.wine_autor;
      wineObj["wine_status"] = wine_information_db.wine_status;

      teste.push(wineObj);
    }

    return teste;

    var jsonStr = [];
    var jsonFinal = [];
    jsonStr = JSON.parse(
      JSON.stringify(establishment_wine_list_info.establishment_listWines)
    );

    for (const [idx, wineObj] of jsonStr.entries()) {
      const wine_information_db = await wines.findById(
        wineObj.establishment_idWine,
        { wine_price: 0, imagesWines: 0 }
      );
      jsonFinal.push({ establishment_wine_list_info, wine_information_db });
    }
    jsonFinal2 = JSON.parse(JSON.stringify(jsonFinal));
    return jsonFinal2;
  } catch (err) {
    console.error(err);
  }
};

exports.listMenuOfEstablishments = async id => {
  try {
    userid = id;
    const resEstablishmentsPre = await establishments.findById(
      id,
      "establishment_listMenu"
    );
    return resEstablishmentsPre;
  } catch (error) {
    console.log(error);
  }
};

exports.findEstablishmentById = async establishment_id => {
  try {
    return await establishments.find({
      _id: establishment_id
    });
  } catch (error) {
    console.log(error);
  }
};

exports.register = async () => {
  try {
    var register = await establishments.find({});

    for (const [idx, wineObj] of register.entries()) {
      var establishment_register_date = moment().subtract(3, "hours");
      var establishment_trial_valid = moment().add(90, "days");

      var data = {
        establishment_name: wineObj.establishment_name,
        establishment_address: wineObj.establishment_address,
        establishment_address2: wineObj.establishment_address2,
        establishment_neighborhood: wineObj.establishment_neighborhood,
        establishment_addressnumber: wineObj.establishment_addressnumber,
        establishment_state: wineObj.establishment_state,
        establishment_zipcode: wineObj.establishment_zipcode,
        establishment_city: wineObj.establishment_city,
        establishment_country: wineObj.establishment_country,
        establishment_phone: wineObj.establishment_phone,
        establishment_Xcoordinate: wineObj.establishment_Xcoordinate,
        establishment_Ycoordinate: wineObj.establishment_Ycoordinate,
        establishment_area_code: wineObj.establishment_area_code,
        establishment_country_code: wineObj.establishment_country_code,
        establishment_work_time: wineObj.establishment_work_time,
        establishment_description: wineObj.establishment_description,
        establishment_register: establishment_register_date,
        establishment_owner: "5e264726697e7672acede3b2",
        establishment_cnpj: wineObj.establishment_cnpj,
        establishment_bank: wineObj.establishment_bank,
        establishment_account_bank: wineObj.establishment_account_bank,
        establishment_account_digit_bank:
          wineObj.establishment_account_digit_bank,
        establishment_images: wineObj.establishment_images[0],
        establishment_logo: wineObj.establishment_logo,
        establishment_description: wineObj.establishment_description,
        establishment_cnpj: wineObj.establishment_cnpj,
        establishment_bank: wineObj.establishment_bank,
        establishment_account_bank: wineObj.establishment_account_bank,
        establishment_account_digit_bank:
          wineObj.establishment_account_digit_bank,
        establishment_trial_valid: establishment_trial_valid
      };
      const establishment = new establishments(data);
      await establishment.save();
    }
  } catch (error) {
    console.log(error);
  }
};
