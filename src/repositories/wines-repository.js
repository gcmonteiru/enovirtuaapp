const mongoose = require("mongoose");
const wines = require("../models/wines");
mongoose.model("Wines");

exports.listWines = async (limit, skip) => {
  try {
    var res = await wines.find().limit(Number(limit)).skip(Number(skip));
    var resFinal = res;
    return resFinal;
  } catch (error) {
    return error;
  }
};

exports.searchWines = async (search, skip, limit) => {
  
  var searchWine = await wines.find({ "wine_name" : { $regex: new RegExp(search), $options: 'i' } }).limit(Number(limit)).skip(Number(skip))
  return searchWine
};

