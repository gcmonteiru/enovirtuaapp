var CronJob = require("cron").CronJob;
const mongoose = require("mongoose");
const crypto = require("crypto");
const dynamicCardRoutes = require("../models/dynamicCardRoute");
mongoose.model("dynamicCardRoute");

/*exports.setDynamicRouteCard = async userid => {
  try {
    new CronJob(
      "30 * * * * *",
      function() {
        var id = crypto.randomBytes(20).toString('hex');
        var data = {route_id: id}
        var teste = new dynamicCardRoutes(data);
        await teste.save()
        console.log("route : " + teste);
        return teste
      },
      null,
      true,
      "America/Los_Angeles"
    );
  } catch (error) {
    console.log(error);
  }
};*/
exports.setDynamicRouteCard = async () => {
  try {
        var id = crypto.randomBytes(20).toString('hex');
        var data = {route_id: id}
        var teste = new dynamicCardRoutes(data);
        await teste.save()
        return teste
  } catch (error) {
    console.log(error);
  }
};