const empty = require("is-empty");
const mongoose = require("mongoose").set("debug", false);
const userRepository = require("../repositories/user-repository");
const cartRepository = require("../repositories/cart-repository");
const ebanxRepository = require("../repositories/ebanx-repository");
const establishmentWebRepository = require("../repositories/establishments-web-repository");
const emptyMethod = require("../general-methods/is-empty");
const uuidv1 = require("uuid/v1");
const moment = require("moment");
const order = require("../models/order");
mongoose.model("order");

exports.createOrder = async (
  user_id,
  user_card_id,
  card_cvv,
  booking_observations,
  booking_wine_open,
  booking_payment,
  booking_time
) => {
  try {
    var items_cart = await cartRepository.listCartItems(user_id);
    items_cart = items_cart.user_cart;

    if (empty(items_cart)) {
      return "empty_cart";
    }

    var user_info = await userRepository.findUserById(user_id);
    if (user_info[0].user_country != "Brazil") {
      return "invalid_country";
    }

    if (empty(await userRepository.findCardById(user_id, user_card_id))) {
      return "invalid_card";
    }
    if (booking_wine_open == "true" && booking_payment == "false") {
      return "order_need_payment";
    }

    var order_price = await cartRepository.getCartPrice(user_id); // getting total price order
    var user_card_id = user_card_id;
    var order_id = uuidv1();
    var makePayment = await ebanxRepository.createPayment(
      user_info[0],
      user_card_id,
      order_price,
      order_id,
      items_cart,
      card_cvv
    );
    this.registerOrder(
      user_info[0]._id,
      user_info[0].user_name,
      user_card_id,
      items_cart,
      order_price,
      order_id,
      booking_observations,
      booking_wine_open,
      booking_payment,
      booking_time
    );
    return makePayment;
  } catch (err) {
    console.error(err);
  }
};

exports.registerOrder = async (
  user_id,
  user_name,
  user_card_id,
  user_cart,
  order_price,
  order_status_id,
  booking_observations,
  booking_wine_open,
  booking_payment,
  booking_time
) => {
  try {
    var dateToSplit = String(booking_time).split("T");

    var dateMonthDayYear = dateToSplit[0].split("-");
    var dateMonthDayYear2 =
      dateMonthDayYear[0] +
      "/" +
      dateMonthDayYear[1] +
      "/" +
      dateMonthDayYear[2];

    var dateHourMinute = dateToSplit[1].split(".");

    var dateHourMinute2 = dateHourMinute[0].split(":");

    var m = moment.utc( dateMonthDayYear2 +
      " " +
      dateHourMinute2[0] +
      ":" +
      dateHourMinute2[1] +
      ":00Z", "YYYY-MM-DD  HH:mm:ss");
 
    const order_booking_time = moment(m
    ).subtract(1, "hours");
    console.log(order_booking_time)
    var order_time = moment().subtract(3, "hours");
    var jsonStr = {
      order_items: user_cart,
      order_price: order_price,
      order_owner: user_id,
      order_id: order_status_id,
      order_owner_name: user_name,
      order_status_payment: "approved",
      order_establishment: user_cart[0].establishment_id,
      order_has_payment: booking_payment,
      order_payment_type: "credit card",
      order_time: order_time,
      order_booking_open: booking_wine_open,
      order_booking_time: order_booking_time,
      order_payment_info: user_card_id,
      order_observations: booking_observations
    };

    var registerOrderListener = new order(jsonStr);
    registerOrderListener.save();

    await establishmentWebRepository.setOrderHistoric(
      user_cart[0].establishment_id,
      JSON.parse(JSON.stringify(registerOrderListener))
    );
    await userRepository.setOrderHistoric(
      user_id,
      JSON.parse(JSON.stringify(registerOrderListener))
    );
    await cartRepository.emptyCartItems(user_id)
  } catch (err) {
    console.error(err);
  }
};

exports.cancelOrder = async (establishment_id, order_id, canceled_reason, hash_code) => {
  try {
    var orderIsCanceled = await this.listOrdersById(order_id);
    if (emptyMethod.isEmpty(orderIsCanceled)) {
      return "order_not_exists";
    } else if (
      orderIsCanceled.order_status_establishment == "canceled" &&
      orderIsCanceled.order_canceled_by == "user"
    ) {
      return "already_canceled";
    }

    var cancelOrder = await ebanxRepository.cancelPayment(hash_code)

    var cancelTheOrder = await order.findOneAndUpdate(
      { order_establishment: establishment_id, order_id: order_id },
      {
        $set: {
          order_status_establishment: "canceled",
          order_canceled_by: "user",
          order_canceled_reason: canceled_reason,
          order_status_payment: "refunded"
        }
      }
    );

    var alterOrderStatusUser = await userRepository.alterOrderStatusToCanceled(
      orderIsCanceled.order_owner,
      orderIsCanceled.order_id,
      "user",
      canceled_reason
    );

    var alterOrderStatusEstablishment = await establishmentWebRepository.alterOrderStatusToCanceled(
      orderIsCanceled.order_establishment,
      orderIsCanceled.order_id,
      "user",
      canceled_reason
    );

    return cancelTheOrder;
  } catch (err) {
    console.error(err);
  }
};

exports.listOrdersById = async order_id_find => {
  try {
    var listOrders = await order.find({
      order_id: order_id_find
    });
    return listOrders[0];
  } catch (err) {
    console.error(err);
  }
};
