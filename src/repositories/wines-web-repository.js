const mongoose = require("mongoose");
const wines = require("../models/wines");
mongoose.model("Wines");

exports.listWines = async (limit, skip) => {
  try {
    var res = await wines.find().limit(Number(limit)).skip(Number(skip));
    var resFinal = res;
    return resFinal;
  } catch (error) {
    return error;
  }
};

exports.searchWines = async (search, skip, limit) => {
  
  var searchWine = await wines.find({ "wine_name" : { $regex: new RegExp(search), $options: 'i' } }).limit(Number(limit)).skip(Number(skip))
  return searchWine
};

exports.winesRegister = async data => {
  var jsonStr = [];
  var jsonResponseId = [];
  try {
    
    jsonStr = JSON.parse(JSON.stringify(data));
    console.log(jsonStr)
    if (Object.keys(jsonStr) == "wines") {
      for (const [idx, wineObj] of jsonStr.wines.entries()) {
        const wine = new wines(wineObj);
        await wine.save();
        jsonResponseId.push(wineObj.nameWine + " ");
      }
      return jsonResponseId;
    }
    //return jsonStr;
  } catch (err) {
    console.error(err);
  }

  
};

exports.updateWines = async (id, data) => {
  await wines.findByIdAndUpdate(id, {
    $set: data
  });
};

exports.deleteWines = async id => {
  await wines.findOneAndRemove(id);
};
