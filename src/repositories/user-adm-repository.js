const usersAdm = require("../models/user-adm");
const jwt = require("jsonwebtoken");
const sha1 = require("sha1");
const moment = require("moment");
const emailSender = require("../utils/mail");
const mongoose = require("mongoose").set("debug", false);
require("dotenv").config();
mongoose.model("user");

exports.userLogin = async (data_email, data_pass) => {
  try {
    return (userExists = await usersAdm.find({
      user_email: data_email,
      user_pass: sha1(data_pass)
    }));
  } catch (error) {
    console.log(error);
  }
};

exports.createUsers = async data => {
  var user_name = data.user_name;
  var user_email = data.user_email;
  var user_pass = sha1(data.user_pass);
  var user_born = new Date(data.user_born);
  var user_country = data.user_country;
  var user_city = data.user_city;
  var user_zipcode = data.user_zipcode;
  var user_state = data.user_state;
  var user_addressnumber = data.user_addressnumber;
  var user_address = data.user_address;
  var user_neighborhood = data.user_neighborhood;
  var user_phone = data.user_phone;
  var user_registered = moment().subtract(3, "hours");
  var user_area_code = data.user_area_code;
  var user_country_code = data.user_country_code;
  var user_cpf = data.user_cpf;

  console.log(user_born)
  const user = new usersAdm({
    user_email: user_email,
    user_pass: user_pass,
    user_name: user_name,
    user_born: user_born,
    user_country: user_country,
    user_city: user_city,
    user_zipcode: user_zipcode,
    user_state: user_state,
    user_addressnumber: user_addressnumber,
    user_address: user_address,
    user_neighborhood: user_neighborhood,
    user_phone: user_phone,
    user_area_code: user_area_code,
    user_country_code: user_country_code,
    user_registered: user_registered,
    user_cpf: user_cpf
  });

  await user.save();

  var user_data = await usersAdm.find({ user_email: data.user_email });
  return user_data;
};

exports.confirmUser = async user_id => {
  var userid = "";
  userid = user_id;
  try {
    const user = await usersAdm.findByIdAndUpdate(userid, {
      $set: { user_confirmed: 1 }
    });

    return user;
  } catch (error) {
    return error;
  }
};

exports.sendEmail = async (user_id_for_token, body) => {
  try {
    var user_email = body.user_email;
    var user_name = body.user_name;
    const tokenForUserVerification = jwt.sign(
      { _id: user_id_for_token },
      process.env.SECRET_EMAIL,
      { expiresIn: "20 d" }
    );
  
    const sendConfirmationEmail = await emailSender.emailConfirmationSender(
      user_email,
      user_name,
      tokenForUserVerification
    );
  } catch (error) {
    return error
  }
};

exports.resetPass = async user_email => {
  try {
    // PRECISA TERMINAR
    return user;
  } catch (error) {
    console.log(error);
  }
};

exports.findUserById = async user_id => {
  try {
    return await usersAdm.find({
      _id: user_id
    });
  } catch (error) {
    console.log(error);
  }
};

exports.findUserByEmail = async user_email => {
  try {
    return await usersAdm.find({
      user_email: user_email
    });
  } catch (error) {
    console.log(error);
  }
};

exports.formatBirthDateBrazil = date => {
  try {
    var dataToSplit = new Date(date)
      .toISOString()
      .replace(/T/, " ") // replace T with a space
      .replace(/\..+/, "");

    var dataToSplit = dataToSplit.toString().split(" ");

    dataToSplit = dataToSplit[0].toString().split("-");

    var user_date_born_brazil =
      dataToSplit[2] + "/" + dataToSplit[1] + "/" + dataToSplit[0];

    return user_date_born_brazil;
  } catch (error) {
    console.log(error);
  }
};

exports.setEstablishmentId = async (user_id, establishment_id) => {
  var userid = "";
  userid = user_id;
  jsonStr = { establishment_id: String(establishment_id) };
  try {
    const user = await usersAdm.findByIdAndUpdate(userid, {
      $push: { user_establishments_owner: jsonStr }
    });
  } catch (error) {
    return error;
  }
};
