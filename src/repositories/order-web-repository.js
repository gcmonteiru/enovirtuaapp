const mongoose = require("mongoose").set("debug", false);
const order = require("../models/order");
const ebanxRepository = require("../repositories/ebanx-repository");
const establishmentRepository = require("../repositories/establishments-web-repository");
const emptyMethod = require("../general-methods/is-empty");
const userRepository = require("../repositories/user-repository");
mongoose.model("order");

exports.listPendingOrders = async establishment_id => {
  try {
    var listOrders = await order.find({
      order_establishment: establishment_id,
      order_status_establishment: "pending"
    });
    return listOrders;
  } catch (err) {
    console.error(err);
  }
};

exports.listConfirmedOrders = async establishment_id => {
  try {
    var listOrders = await order.find({
      order_establishment: establishment_id,
      order_status_establishment: "confirmed"
    });
    return listOrders;
  } catch (err) {
    console.error(err);
  }
};

exports.listCanceledOrders = async establishment_id => {
  try {
    var listOrders = await order.find({
      order_establishment: establishment_id,
      order_status_establishment: "canceled"
    });
    return listOrders;
  } catch (err) {
    console.error(err);
  }
};

exports.confirmOrder = async (establishment_id, order_id) => {
  try {
    var orderIsCanceled = await this.listOrdersById(order_id);
    if (emptyMethod.isEmpty(orderIsCanceled)) {
      return "order_not_exists";
    } else if (orderIsCanceled.order_status_establishment == "canceled") {
      return "canceled_error";
    } else if (orderIsCanceled.order_status_establishment == "confirmed") {
      return "already_confirmed";
    }

    var confirmOrder = await order.findOneAndUpdate(
      { order_establishment: establishment_id, order_id: order_id },
      { $set: { order_status_establishment: "confirmed" } }
    );

    return confirmOrder;
  } catch (err) {
    console.error(err);
  }
};

exports.cancelOrder = async (establishment_id, order_id, canceled_reason) => {
  try {
    var orderIsCanceled = await this.listOrdersById(order_id);
    if (emptyMethod.isEmpty(orderIsCanceled)) {
      return "order_not_exists";
    } else if (orderIsCanceled.order_status_establishment == "canceled" && orderIsCanceled.order_canceled_by == "establishment") {
      return "already_canceled";
    }else if (orderIsCanceled.order_status_establishment == "canceled" && orderIsCanceled.order_canceled_by == "user") {
      return "already_canceled";
    }

    var cancelOrder = await ebanxRepository.cancelPayment(hash_code)

    var cancelTheOrder = await order.findOneAndUpdate(
      { order_establishment: establishment_id, order_id: order_id },
      { $set: { order_status_establishment: "canceled", order_canceled_by: "establishment", order_canceled_reason: canceled_reason } }
    );

    var alterOrderStatusUser = await userRepository.alterOrderStatusToCanceled(
      orderIsCanceled.order_owner,
      orderIsCanceled.order_id,
      "establishment",
      canceled_reason
    );

    var alterOrderStatusEstablishment = await establishmentRepository.alterOrderStatusToCanceled(
      orderIsCanceled.order_establishment,
      orderIsCanceled.order_id,
      "establishment",
      canceled_reason
    );

    return cancelTheOrder;
  } catch (err) {
    console.error(err);
  }
};

exports.listOrdersById = async order_id_find => {
  try {
    var listOrders = await order.find({
      order_id: order_id_find
    });
    return listOrders[0];
  } catch (err) {
    console.error(err);
  }
};
