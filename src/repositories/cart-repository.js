const mongoose = require("mongoose").set("debug", false);
const users = require("../models/user");
mongoose.model("user");

exports.createCartItems = async (data, user_id) => {
  try {
    var jsonStr = [];
    var jsonResponseId = [];
    jsonStr = JSON.parse(JSON.stringify(data));

    await users.findByIdAndUpdate(user_id, {
      $push: { user_cart: jsonStr }
    });

    priceCart = await this.getCartPrice(user_id);
    await users.findByIdAndUpdate(user_id, {
      $set: { user_cart_price: priceCart }
    });
    jsonResponseId.push(jsonStr[0].item_id);

    return jsonResponseId;
  } catch (err) {
    console.error(err);
  }
};

exports.emptyCartItems = async user_id => {
  try {
    await users.findByIdAndUpdate(user_id, {
      $unset: { user_cart: "" }
    });
    await users.findByIdAndUpdate(user_id, {
      $set: { user_cart_price: "0" }
    });
    return true;
  } catch (err) {
    console.error(err);
  }
};

exports.listCartItems = async user_id => {
  try {
    listCart = await users
      .findById(user_id)
      .select({ user_cart: 1, _id: 0, user_cart_price: 1 });
    return listCart;
  } catch (err) {
    console.error(err);
  }
};

exports.getCartPrice = async user_id => {
  var jsonStr = [];
  var priceSum = 0;
  try {
    var items_cart = await this.listCartItems(user_id); // getting items of cart
    items_cart = items_cart.user_cart; // setting JSON

    jsonStr = JSON.parse(JSON.stringify(items_cart));
    for (const [idx, itemObj] of jsonStr.entries()) {
      priceSum = Number(itemObj.item_price) + priceSum;
    }
    return String(priceSum);
  } catch (err) {
    console.error(err);
  }
};
