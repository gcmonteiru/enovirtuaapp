const mongoose = require("mongoose").set("debug", false);
const rp = require("request-promise");
const userController = require("../controllers/user-controller");
const ebanxmodel = require("../models/ebanx-orders");
const decrypter = require("../encrypter/encrypter");
require("dotenv").config();
mongoose.model("ebanx");

exports.getStatusOrder = async hash_codes => {
  try {
    var integrationKey = process.env.EBANX_KEY;
    var requestOpts = {
      encoding: "utf8",
      uri:
        "https://staging.ebanx.com.br/ws/query?integration_key=" +
        integrationKey +
        "&hash=" +
        hash_codes,
      method: "POST",
      json: false
    };

    var ebanxResponse = await rp(requestOpts)
      .then(async function(body) {
        return body;
      })
      .catch(function(err) {
        console.log(console.dir);
      });
    var ebanxResponseObj = JSON.parse(ebanxResponse);
    var ebanxSave = new ebanxmodel(ebanxResponseObj);
    await ebanxSave.save();
  } catch (error) {
    console.error(error);
  }
};

exports.createPayment = async (
  data,
  user_card_id,
  order_price,
  order_id_number,
  items_cart,
  card_cvv_security
) => {
  try {
    var key = process.env.EBANX_KEY;
    var user_name = data.user_name;
    var user_email = data.user_email;
    var user_country = data.user_country;
    var user_city = data.user_city;
    var user_zipcode = data.user_zipcode;
    var user_state = data.user_state;
    var user_addressnumber = data.user_addressnumber;
    var user_address = data.user_address;
    var user_address2 = data.user_address2;
    var user_neighborhood = data.user_neighborhood;
    var user_phone = data.user_phone;
    var user_area_code = data.user_area_code;
    var user_country_code = data.user_country_code;
    var user_cpf = data.user_cpf;
    var user_card = await userController.findCardById(data._id, user_card_id);
    var oder_price = order_price;
    var cart_items = items_cart;
    var order_id = order_id_number;
    var card_cvv = card_cvv_security;

    var dataString = {
      integration_key: key,
      operation: "request",
      payment: {
        name: user_name,
        email: user_email,
        document: user_cpf,
        address: user_address,
        street_number: user_addressnumber,
        city: user_city,
        state: user_state,
        zipcode: user_zipcode,
        street_complement: user_address2 + "," + user_neighborhood,
        country: "br",
        phone_number: user_area_code + user_phone,
        payment_type_code: "creditcard",
        merchant_payment_code: order_id,
        currency_code: "BRL",
        instalments: 1,
        amount_total: oder_price,
        creditcard: {
          card_number: decrypter.decrypt(user_card.card_if).substr(0, 16),
          card_name: user_card.card_name,
          card_due_date:
            decrypter.decrypt(user_card.card_if).substr(16, 2) +
            "/" +
            decrypter.decrypt(user_card.card_if).substr(18, 4),
          card_cvv: card_cvv
        }
      }
    };

    var requestOpts = {
      encoding: "utf8",
      uri: "https://staging.ebanx.com.br/ws/direct",
      method: "POST",
      body: dataString,
      json: true
    };

    var ebanxResponse = await rp(requestOpts)
      .then(async function(body) {
        return body;
      })
      .catch(function(err) {
        console.log(console.dir);
      });
    return ebanxResponse;
  } catch (error) {
    console.log(error);
  }
};

exports.cancelPayment = async (hash_codes) => {
  try {

    var integrationKey =process.env.EBANX_KEY;
    var requestOpts = {
      encoding: "utf8",
      uri:
        "https://staging.ebanx.com.br/ws/integration_key=" +
        integrationKey +
        "&hash=" +
        hash_codes,
      method: "POST",
      json: false
    };

    var ebanxResponse = await rp(requestOpts)
      .then(async function(body) {
        return body;
      })
      .catch(function(err) {
        console.log(console.dir);
      });
    var ebanxResponseObj = JSON.parse(ebanxResponse);
    return ebanxResponseObj
  } catch (error) {
    console.log(error);
  }
};
