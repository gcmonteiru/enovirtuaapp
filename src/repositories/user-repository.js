const users = require("../models/user");
const encryptor = require("../encrypter/encrypter");
const emptyMethod = require("../general-methods/is-empty");
const jwt = require("jsonwebtoken");
const sha1 = require("sha1");
const emailSender = require("../utils/mail");
require("dotenv").config();
const mongoose = require("mongoose").set("debug", true);
mongoose.model("user");

exports.userLogin = async (data_user_email, data_user_pass) => {
  try {
    return (userExists = await users.find(
      {
        user_email: data_user_email,
        user_pass: sha1(data_user_pass)
      },
      {
        user_credit_card: 0,
        user_orders_historic: 0,
        user_cart_price: 0,
        user_cart: 0
      }
    ));
  } catch (error) {
    console.log(error);
  }
};

exports.createUsers = async data => {
  const moment = require("moment");
  const user_born_edited = moment.utc(data.user_born + " " + "00:00:00Z",  "DD-MM-YYYY  HH:mm:ss")

  var user_name = data.user_name;
  var user_email = data.user_email;
  var user_pass = sha1(data.user_pass);
  var user_born = user_born_edited;
  var user_country = data.user_country;
  var user_city = data.user_city;
  var user_zipcode = data.user_zipcode;
  var user_state = data.user_state;
  var user_addressnumber = data.user_addressnumber;
  var user_address = data.user_address;
  var user_neighborhood = data.user_neighborhood;
  var user_phone = data.user_phone;
  var user_registered = Date.now();
  var user_area_code = data.user_area_code;
  var user_country_code = data.user_country_code;
  var user_cpf = data.user_cpf;

  const user = new users({
    user_email: user_email,
    user_pass: user_pass,
    user_name: user_name,
    user_born: user_born,
    user_country: user_country,
    user_city: user_city,
    user_zipcode: user_zipcode,
    user_state: user_state,
    user_addressnumber: user_addressnumber,
    user_address: user_address,
    user_neighborhood: user_neighborhood,
    user_phone: user_phone,
    user_area_code: user_area_code,
    user_country_code: user_country_code,
    user_registered: user_registered,
    user_cpf: user_cpf
  });

  await user.save();

  var user_data = await users.find({ user_email: data.user_email });
  return user_data;
};

exports.confirmUser = async user_id => {
  var userid = "";
  userid = user_id;
  try {
    const user = await users.findByIdAndUpdate(userid, {
      $set: { user_confirmed: 1 }
    });

    return user;
  } catch (error) {
    return error;
  }
};

exports.sendEmail = async (data, user_id_for_token) => {
  var user_email = data.user_email;
  var user_name = data.user_name;

  const tokenForUserVerification = jwt.sign(
    { _id: user_id_for_token },
    process.env.SECRET_EMAIL,
    { expiresIn: "20 d" }
  );

  const sendConfirmationEmail = await emailSender.emailConfirmationSender(
    user_email,
    user_name,
    tokenForUserVerification
  );
};

exports.resetPass = async (user_email, user_id, user_name) => {
  try {
    const tokenForUserVerification = jwt.sign(
      { _id: user_id, user_email: user_email },
      process.env.SECRET_RESET_PASS,
      { expiresIn: "1 h" }
    );

    const sendResetPassEmail = await emailSender.passResetEmailSender(
      user_email,
      user_name,
      tokenForUserVerification
    );
  } catch (error) {
    console.log(error);
  }
};

exports.findUserById = async user_id => {
  try {
    return await users.find({
      _id: user_id
    });
  } catch (error) {
    console.log(error);
  }
};

exports.findUserByEmail = async user_email => {
  try {
    return await users.find({
      user_email: user_email
    });
  } catch (error) {
    console.log(error);
  }
};

exports.formatBirthDateBrazil = date => {
  try {
    var dataToSplit = new Date(date)
      .toISOString()
      .replace(/T/, " ") // replace T with a space
      .replace(/\..+/, "");

    var dataToSplit = dataToSplit.toString().split(" ");

    dataToSplit = dataToSplit[0].toString().split("-");

    var user_date_born_brazil =
      dataToSplit[2] + "/" + dataToSplit[1] + "/" + dataToSplit[0];

    return user_date_born_brazil;
  } catch (error) {
    console.log(error);
  }
};

exports.userSetCard = async (user_id, data) => {
  try {
    var cardExists = await this.findCardById(
      user_id,
      String(data.user_card_number).substr(12, 4)
    );

    if (data.user_card_number.toString().length != 16) {
      return "lenght_invalid";
    } else if (emptyMethod.isEmpty(data.user_card_number)) {
      return "lenght_invalid";
    }

    if (emptyMethod.isEmpty(cardExists)) {
      var card_name = data.user_card_name;
      var card_last4numbers = String(data.user_card_number).substr(12, 4);
      var card_bin = String(data.user_card_number).substr(0, 6);
      var dataCardMerge =
        data.user_card_number +
        data.user_expiration_month +
        data.user_expiration_year;
      var dataCardEncrypted = encryptor.encrypt(dataCardMerge);
      var dataCardJson = {
        card_name: card_name,
        card_if: dataCardEncrypted,
        card_brand: data.user_card_brand,
        card_4numbers: card_last4numbers,
        card_bin: card_bin
      };

      await users.findByIdAndUpdate(user_id, {
        $push: { user_credit_card: dataCardJson }
      });
      return;
    }
    return "card_exists";
  } catch (error) {
    console.log(error);
  }
};

exports.setOrderHistoric = async (user_id, data) => {
  try {
    var register = await users.findByIdAndUpdate(user_id, {
      $push: { user_orders_historic: data }
    });
  } catch (err) {
    console.error(err);
  }
};

exports.alterOrderStatusToCanceled = async (
  user_id,
  order_id_find,
  canceled_by,
  cancel_reason
) => {
  try {
    await users.updateOne(
      { _id: user_id, "user_orders_historic.order_id": order_id_find },
      {
        $set: {
          "user_orders_historic.$.order_status_payment": "refunded",
          "user_orders_historic.$.order_status_establishment": "canceled",
          "user_orders_historic.$.order_canceled_by": canceled_by,
          "user_orders_historic.$.order_canceled_reason": cancel_reason
        }
      }
    );
  } catch (err) {
    console.error(err);
  }
};

exports.getUserOrdersHistoric = async user_id => {
  try {
    var getOrders = await users
      .find({
        _id: user_id
      })
      .select({ user_orders_historic: 1, _id: 0 });
    return getOrders[0].user_orders_historic;
  } catch (error) {
    console.log(error);
  }
};

exports.getUserCards = async user_id => {
  try {
    console.log(user_id);
    var getUserCards = await users
      .find({
        _id: user_id
      })
      .select({ user_credit_card: 1, _id: 0 });
    console.log(getUserCards[0].user_credit_card);
    return getUserCards[0].user_credit_card;
  } catch (error) {
    console.log(error);
  }
};

exports.deleteUserCards = async (user_id, last4) => {
  try {
    var card_last4numbers = await this.getUserCards(user_id);
    var cardExists = false;

    jsonStr = JSON.parse(JSON.stringify(card_last4numbers));

    for (const [idx, itemObj] of jsonStr.entries()) {
      if (itemObj.card_4numbers == last4) {
        cardExists = true;
      }
    }

    if (cardExists) {
      var deleteCard = await users.findByIdAndUpdate(user_id, {
        $pull: { user_credit_card: { card_4numbers: last4 } }
      });
      return true;
    }
    return false;
  } catch (error) {
    console.log(error);
  }
};

exports.findCardById = async (user_id, idcard) => {
  try {
    var card_last4numbers = await this.getUserCards(user_id);
    var jsonCardObj = [];

    jsonStr = JSON.parse(JSON.stringify(card_last4numbers));
    for (const [idx, itemObj] of jsonStr.entries()) {
      if (itemObj.card_4numbers == idcard) {
        jsonCardObj.push(itemObj);
      }
    }
    return jsonCardObj[0];
  } catch (error) {
    return error;
  }
};
