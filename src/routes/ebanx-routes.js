const ebanxController = require("../controllers/ebanx-controller");
const express = require("express");
const router = express.Router();

router.post(
  "/status_change/operation=:operation&notification_type=:notification_type&hash_codes=:hash_codes",
  ebanxController.getStatusOrder
);

module.exports = router;
