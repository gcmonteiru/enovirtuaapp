const winesController = require("../controllers/wines-web-controller");
const express = require("express");
const router = express.Router();

router.get("/list-wines/:limit&:skip", winesController.listWines);

router.post("/list-wines/search-wines/:limit&:skip", winesController.searchWines);

router.post("/register-wines", winesController.registerWines);

router.put("/alter-wines/:id", winesController.updateWines);

router.delete("/delete-wines/:id", winesController.deleteWines);

module.exports = router;
