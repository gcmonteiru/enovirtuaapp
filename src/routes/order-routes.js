const orderController = require("../controllers/order-controller");
const dataChecker = require("../general-methods/data-checker");
const express = require("express");
const router = express.Router();

router.post(
  "/create-order/",
  async function(req, res, next) {
    try {
      req.jwtInfo = req.jwtInfo;
      var dataCheckerEmptyVar = dataChecker.orderDataEmptyChecker(req.body);
      var dataCheckerValidVar = dataChecker.orderDataValidChecker(req.body);
      var dataCheckerLogicVar = dataChecker.orderDataLogicChecker(req.body);
      var dateCheckerValidVar = dataChecker.orderDateValidChecker(req.body);
      console.log(dataCheckerValidVar)
      if (dataCheckerEmptyVar) {
        return res.status(400).send({
          error: true,
          message:
            "Erro ao processar compra, existem campos vazios na requisição."
        });
      } else if (dateCheckerValidVar) {
        return res.status(400).send({
          error: true,
          message:
            "Erro ao processar compra, a data de reserva é invalida."
        });
      } else if (dataCheckerValidVar) {
        return res.status(400).send({
          error: true,
          message:
            "Erro ao processar compra, existem campos invalidos na requisição."
        });
      } else if (dataCheckerLogicVar) {
        return res.status(400).send({
          error: true,
          message:
            "Erro ao processar compra, para solicitar a abertura do vinho, é necessário que o booking_payment seja true."
        });
      }
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro: " + error });
    }
  },
  orderController.createOrder
);

router.get(
  "/list-orders/",
  async function(req, res, next) {
    try {
      req.jwtInfo = req.jwtInfo;
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro: " + error });
    }
  },
  orderController.listOrders
);

router.put(
  "/cancel-order/:idestablishment",
  async function(req, res, next) {
    try {
      req.jwtInfo = req.jwtInfo;
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro: " + error });
    }
  },
  orderController.cancelOrder
);

module.exports = router;
