const express = require("express");
const router = express.Router();
const establishmentController = require("../controllers/establishments-controller");

router.put(
  "/nearby-establishments/:limit&:skip", 
  establishmentController.listEstablishments
);
router.post(
  "/search-establishments/:limit&:skip",
  establishmentController.searchEstablishment
);

router.get(
  "/list-wine-establishments/:idEstablishment",
  establishmentController.listWinesofEstablishments
);

router.get(
  "/get-establishments-by-id/:idEstablishment",
  establishmentController.findEstablishmentById
);

router.get(
  "/list-menu-establishments/:idEstablishment",
  establishmentController.listMenuOfEstablishments
);

router.get(
  "/register",
  establishmentController.register
);

module.exports = router;
