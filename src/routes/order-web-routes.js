const orderWebController = require("../controllers/order-web-controller");
const dataChecker = require("../general-methods/data-checker");
const express = require("express");
const router = express.Router();

router.get(
  "/list-pending-orders/:idestablishment",
  async function(req, res, next) {
    try {
      req.jwtInfo = req.jwtInfo;
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro: " + error });
    }
  },
  orderWebController.listPendingOrders
);

router.get(
  "/list-confirmed-orders/:idestablishment",
  async function(req, res, next) {
    try {
      req.jwtInfo = req.jwtInfo;
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro: " + error });
    }
  },
  orderWebController.listConfirmedOrders
);

router.get(
  "/list-canceled-orders/:idestablishment",
  async function(req, res, next) {
    try {
      req.jwtInfo = req.jwtInfo;
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro: " + error });
    }
  },
  orderWebController.listCanceledOrders
);


router.put(
  "/confirm-order/:idestablishment",
  async function(req, res, next) {
    try {
      req.jwtInfo = req.jwtInfo;
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro: " + error });
    }
  },
  orderWebController.confirmOrder
);


router.put(
  "/cancel-order/:idestablishment",
  async function(req, res, next) {
    try {
      req.jwtInfo = req.jwtInfo;
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro: " + error });
    }
  },
  orderWebController.cancelOrder
);



module.exports = router;
