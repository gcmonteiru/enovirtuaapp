const userController = require("../controllers/user-controller");
const jwt_info_checker = require("../general-methods/jwt-info-checker");
const jwt = require("jsonwebtoken");
require("dotenv").config();
const express = require("express");
const router = express.Router();

router.post(
  "/login",
  function(req, res, next) {
    try {
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro" + error });
    }
  },
  userController.loginUser
);

router.post(
  "/create",
  function(req, res, next) {
    try {
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro" + error });
    }
  },
  userController.createUser
);

router.get(
  "/confirm/:token",
  function(req, res, next) {
    try {
      const jwtString = req.params.token;
      if (jwt.verify(jwtString, process.env.SECRET_EMAIL)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_EMAIL);
        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  userController.confirmUser
);

router.get(
  "/pass/reset",
  function(req, res, next) {
    try {
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_EMAIL)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_EMAIL);
        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  userController.resetPass
);

router.post(
  "/card/register",
  function(req, res, next) {
    try {
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_API)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_API);
        var isJwtInfoCorrect = jwt_info_checker.verifyUserIdEmail(req.jwtInfo);
        if (isJwtInfoCorrect.error == "true") {
          res.status(400).send(isJwtInfoCorrect);
          return;
        }
        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  userController.userSetCard
);

router.get(
  "/card/get",
  function(req, res, next) {
    try {
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_API)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_API);
        var isJwtInfoCorrect = jwt_info_checker.verifyUserIdEmail(req.jwtInfo);
        if (isJwtInfoCorrect.error == "true") {
          res.status(400).send(isJwtInfoCorrect);
          return;
        }
        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  userController.getUserCards
);

router.delete(
  "/card/delete/:card",
  function(req, res, next) {
    try {
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_API)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_API);
        var isJwtInfoCorrect = jwt_info_checker.verifyUserIdEmail(req.jwtInfo);
        if (isJwtInfoCorrect.error == "true") {
          res.status(400).send(isJwtInfoCorrect);
          return;
        }
        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  userController.deleteUserCards
);

router.get(
  "/find-user-by-id",
  function(req, res, next) {
    try {
      // Validating if token is ok
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_API)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_API);
        var isJwtInfoCorrect = jwt_info_checker.verifyUserIdEmail(req.jwtInfo);
        if (isJwtInfoCorrect.error == "true") {
          res.status(400).send(isJwtInfoCorrect);
          return;
        }
        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  userController.findUserById
);

router.get(
  "/find-user-by-email",
  function(req, res, next) {
    try {
      // Validating if token is ok
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_API)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_API);
        var isJwtInfoCorrect = jwt_info_checker.verifyUserIdEmail(req.jwtInfo);
        if (isJwtInfoCorrect.error == "true") {
          res.status(400).send(isJwtInfoCorrect);
          return;
        }
        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  userController.findUserById
);

module.exports = router;
