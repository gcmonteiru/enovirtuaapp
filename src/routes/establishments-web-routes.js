const express = require("express");
const router = express.Router();
const dataChecker = require("../general-methods/data-checker");
const establishmentController = require("../controllers/establishments-web-controller");

router.post(
  "/register-establishments",
  establishmentController.createEstablishments
);

router.put(
  "/register-establishments/upload-images/:idEstablishment",
  establishmentController.createImagesEstablishments
);
router.put(
  "/update-establishments/:id",
  establishmentController.updateEstablishments
);

router.put(
  "/update-wine-of-establishments/:idEstablishment&:idWine&:priceWine",
  async function(req, res, next) {
    try {
 
      var dataCheckerEmptyVar = dataChecker.wineDataEmptyChecker(req.params);

      if (dataCheckerEmptyVar) {
        return res.status(400).send({
          error: true,
          message:
            "Erro ao processar atualização, existem campos vazios na requisição."
        });
      }
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro: " + error });
    }
  },
  establishmentController.updateWineOfEstablishment
);

router.put(
  "/insert-wine-establishments/:idEstablishment&:idWine&:priceWine",
  establishmentController.insertWineEstablishment
);
router.get(
  "/list-wine-establishments/:idEstablishment",
  establishmentController.listWinesofEstablishments
);

router.get(
  "/get-establishments-by-id/:idEstablishment",
  establishmentController.findEstablishmentById
);

router.delete(
  "/delete-establishments/:id",
  establishmentController.deleteEstablishments
);

module.exports = router;
