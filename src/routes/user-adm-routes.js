const jwt_info_checker = require("../general-methods/jwt-info-checker");
const userAdmController = require("../controllers/user-adm-controller");
const jwt = require("jsonwebtoken");
require("dotenv").config();
const express = require("express");
const router = express.Router();

router.post(
  "/login",
  function(req, res, next) {
    try {
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro" + error });
    }
  },
  userAdmController.loginUserAdm
);

router.post(
  "/create",
  function(req, res, next) {
    try {
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro" + error });
    }
  },
  userAdmController.createUserAdm
);

router.get(
  "/confirm/:token",
  function(req, res, next) {
    try {
      const jwtString = req.params.token;
      if (jwt.verify(jwtString, process.env.SECRET_EMAIL)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_EMAIL);
        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  userAdmController.confirmUserAdm
);

router.post(
  "/pass/reset",
  function(req, res, next) {
    try {
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_WEB)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_WEB);
        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  userAdmController.resetPassAdm
);

module.exports = router;
