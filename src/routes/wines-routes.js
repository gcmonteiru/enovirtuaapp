const winesController = require("../controllers/wines-controller");
const express = require("express");
const router = express.Router();

router.get("/list-wines/:limit&:skip", winesController.listWines);

router.post("/list-wines/search-wines/:limit&:skip", winesController.searchWines);


module.exports = router;
