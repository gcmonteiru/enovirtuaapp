const cartController = require("../controllers/cart-controller");
const express = require("express");
const checkerDataMethod = require("../general-methods/data-checker");
const router = express.Router();


/* ROUTES CARREGA OS CHECKERS DE BODY E PARAMS */

router.post(
  "/items/create-cart",
  async function(req, res, next) {
    try {
      data = req.body;

      var dataEmptyChecker = checkerDataMethod.cartDataEmptyChecker(data);

      if (dataEmptyChecker) {
        return res.status(400).send({
          error: "true",
          message:
            "Erro ao tentar adicionar item no carrinho, existem campos vazios."
        });
      }
      var dataValidChecker = await checkerDataMethod.cartDataValidChecker(data);
      if (dataValidChecker) {
        return res.status(400).send({
          error: "true",
          message:
            "Erro ao tentar adicionar item no carrinho, id do establishment invalido."
        });
      }

      req.jwtInfo = req.jwtInfo;
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro: " + error });
    }
  },
  cartController.createCartItems
);

router.delete(
  "/items/empty-cart",
  function(req, res, next) {
    try {
      req.jwtInfo = req.jwtInfo;
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro: " + error });
    }
  },
  cartController.emptyCartItems
);

router.get(
  "/items/list-cart",
  function(req, res, next) {
    try {
      req.jwtInfo = req.jwtInfo;
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro: " + error });
    }
  },
  cartController.listCartItems
);
module.exports = router;
