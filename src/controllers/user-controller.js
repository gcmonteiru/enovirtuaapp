const userRepository = require("../repositories/user-repository");
const path = require("path");
const emptyMethod = require("../general-methods/is-empty");
const jwt = require("jsonwebtoken");
const empty = require("is-empty");
const mongoose = require("mongoose");
require("dotenv").config();
mongoose.model("user");

exports.loginUser = async (req, res) => {
  try {
    const userExists = await userRepository.userLogin(
      req.body.user_email,
      req.body.user_pass
    );
    var userInformation = userExists[0];
    if (empty(userInformation)) {
      res.status(400).send({
        error: "true",
        message: "User não encontrado",
        error_msg: userInformation
      });
    }

    if (userInformation.user_confirmed == 0) {
      res.status(400).send({
        error: "true",
        message: "User não confirmado",
        userInformation
      });
    }

    const tokenForUser = jwt.sign({ _id: userInformation._id, user_email: userInformation.user_email }, process.env.SECRET_API, {
      expiresIn: "1 d"
    });

    res.status(201).send({
      error: "false",
      message: "User logado",
      userInformation,
      token: tokenForUser
    });
  } catch (error) {
    res.status(500).send({ message: "Erro" + error });
  }
};

exports.createUser = async (req, res) => {
  try {
    const userExists = await userRepository.findUserByEmail(req.body.user_email);

    if (empty(userExists)) {
      var userCreate = await userRepository.createUsers(req.body);
      var userInformation = userCreate[0];
      let user_id_for_token = userInformation._id;

      await userRepository.sendEmail(req.body, user_id_for_token);
      res.status(201).send({
        error: "false",
        message:
          "User cadastrado. O user foi registrado. Para efetuar o login, o user precisa validar o e-mail enviado na caixa de e-mail cadastrada."
      });
    } else {
      res.status(400).send({
        error: "true",
        message: "User já registrado"
      });
    }
  } catch (error) {
    res.status(500).send({ message: "Erro" + error });
  }
};

exports.confirmUser = async (req, res) => {
  var user_id = req.jwtInfo._id;
  try {
    await userRepository.confirmUser(user_id);
    res.sendFile(path.resolve("src/html" + "/confirm_user.html"));
  } catch (error) {
    return error;
  }
};

exports.resetPass = async (req, res) => {
  var user_email = req.body.user_email;
  try {
    var user_info = await userRepository.findUserByEmail(user_email)
    console.log(user_info)
    if(emptyMethod.isEmpty(user_info[0])){
      res.status(400).send({
        error: "true",
        message: "E-mail não encontrado nos nossos registros"
      });
    }
    var resetPass = await userRepository.resetPass(user_info[0].user_email, user_info[0]._id, user_info[0].user_name);
    //res.status(201).send({ message: resetPass });
  } catch (error) {
    return error;
  }
};

exports.userSetCard = async (req, res) => {
  var user_id = req.jwtInfo._id;
  try {
    var userSetCard = await userRepository.userSetCard(user_id, req.body);
    if(userSetCard == "card_exists"){
      return res
      .status(400)
      .send({
        error: true,
        message:
          "Erro ao adicionar cartão, verificamos que existe um cartão com o mesmo final."
      });
    }else if(userSetCard == "lenght_invalid"){
      return res
      .status(400)
      .send({
        error: true,
        message:
          "Erro ao adicionar cartão, o cartão é invalido."
      });
    }
    res.status(201).send({ message: "Card cadastrado" });
  } catch (error) {
    return error;
  }
};

exports.getUserCards = async (req, res) => {
  var user_id = req.jwtInfo._id;
  try {
    var cardInfo = await userRepository.getUserCards(user_id);
    console.log(cardInfo)
    if(emptyMethod.isEmpty(cardInfo)){
      return res
      .status(400)
      .send({
        error: true,
        message:
          "Não existe nenhum cartão cadastrado."
      });
    }
    res.status(201).send({ error: "false", cardInfo });
  } catch (error) {
    return error;
  }
};

exports.deleteUserCards = async (req, res) => {
  var user_id = req.jwtInfo._id;
  var card_last4 = req.params.card
  try {
    if(await userRepository.deleteUserCards(user_id, card_last4) == true){
      res.status(201).send({ error: "false", message: "Cartão deletado" });
    }
    res.status(400).send({ error: "true", message: "O cartão não foi deletado, verifique se o id está correto ou se o mesmo é existente." });
  } catch (error) {
    return error;
  }
};

exports.findUserById = async (req, res) => {
  var user_id = req.userid._id;
  try {
    var userInfo = await userRepository.findUserById(user_id);
    res.status(201).send({ userInfo });
  } catch (error) {
    console.log(error);
  }
};


exports.findUserByEmail = async (req, res) => {
  var user_email = req.jwtInfo.user_email;
  try {
    var userInfo = await userRepository.findUserByEmail(user_email);
    res.status(201).send({ userInfo });
  } catch (error) {
    console.log(error);
  }
};


exports.findCardById = async (userid, idcard) => {
  try {
    var cardInfo = await userRepository.findCardById(userid, idcard);
    return cardInfo
  } catch (error) {
    return error;
  }
};
