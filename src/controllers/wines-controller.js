const repository = require("../repositories/wines-repository");
const emptyMethod = require("../general-methods/is-empty");

exports.listWines = async (req, res) => {
  try {
    var limit = req.params.limit;
    var skip = req.params.skip;
    const data = await repository.listWines(limit, skip);
    res.status(200).send(data);
  } catch (e) {
    res.status(500).send({ message: "Foi encontrado um erro ao listar os vinhos", error: e });
  }
};

exports.searchWines = async (req, res) => {
  try {
    var skip = req.params.skip
    var limit = req.params.limit
    var wine = req.body.wine_search;
    if (emptyMethod.isEmpty(wine)) {
      return res.status(400).send({
        error: "true",
        message: "Erro ao procurar vinhos, campo de busca vazio."
      });
    }
    const data = await repository.searchWines(wine, skip, limit);

    if (emptyMethod.isEmpty(data)) {
      return res.status(400).send({
        error: "true",
        message: "Nenhum estabelecimento foi encontrado."
      });
    }
    res.status(200).send(data);
  } catch (e) {
    res
      .status(500)
      .send({ message: "Falha ao carregar os estabelecimentos!" + e });
  }
};
