const ebanxRepository = require("../repositories/ebanx-repository");

exports.getStatusOrder = async (req, res) => {
  try {
    var operation = req.params.operation;
    var notification_type = req.params.notification_type;
    var hash_codes = req.params.hash_codes
    var statusOrder = await ebanxRepository.getStatusOrder(hash_codes)

    res.status(200).send({error: "false", message: "ok"});
  } catch (error) {
    return error;
  }
};
