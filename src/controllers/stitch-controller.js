const express = require('express');
const router = express.Router();
const {  
  Stitch, 
  AnonymousCredential,
} = require('mongodb-stitch-server-sdk');


const client = Stitch.defaultAppClient;
exports.stitchAuth = function (req, res, next) {
  
  

    client.auth.loginWithCredential(new AnonymousCredential()).then(user => {
      console.log(`Logged in as anonymous user with id: ${user.id}`)
      res.status(200).send({
        title: `Logged in as anonymous user with id: ${user.id}`,
        version: '1.0.0'
      });
    });



}

