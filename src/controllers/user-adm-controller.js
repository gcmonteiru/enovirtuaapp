const userAdmRepository = require("../repositories/user-adm-repository");
const path = require("path");
const jwt = require("jsonwebtoken");
const emptyMethod = require("../general-methods/is-empty");
const mongoose = require("mongoose");
require("dotenv").config();
mongoose.model("user");

exports.loginUserAdm = async (req, res) => {
  try {
    const userExists = await userAdmRepository.userLogin(
      req.body.user_email,
      req.body.user_pass
    );
    var userInformation = userExists[0];
    if (emptyMethod.isEmpty(userInformation)) {
      res.status(400).send({
        error: "true",
        message: "User não encontrado",
        error_msg: userInformation
      });
    }

    if (userInformation.user_confirmed == 0) {
      res.status(400).send({
        error: "true",
        message: "User não confirmado",
        userInformation
      });
    }
    
    const tokenForUser = jwt.sign(
      {
        _id: userInformation._id,
        user_email: userInformation.user_email
      },
      process.env.SECRET_API,
      {
        expiresIn: "1 d"
      }
    );

    res.status(201).send({
      error: "false",
      message: "User logado",
      userInformation,
      token: tokenForUser
    });
  } catch (error) {
    res.status(500).send({ message: "Erro" + error });
  }
};

exports.createUserAdm = async (req, res) => {
  try {
    const userExists = await userAdmRepository.findUserByEmail(
      req.body.user_email
    );

    if (emptyMethod.isEmpty(userExists)) {
      var userCreate = await userAdmRepository.createUsers(req.body);
      var userInformation = userCreate[0];
      let user_id_for_token = userInformation._id;

      await userAdmRepository.sendEmail(user_id_for_token, req.body);
      res.status(201).send({
        error: "false",
        message:
          "User cadastrado. O user foi registrado. Para efetuar o login, o user precisa validar o e-mail enviado na caixa de e-mail cadastrada."
      });
    } else {
      res.status(400).send({
        error: "true",
        message: "User já registrado"
      });
    }
  } catch (error) {
    res.status(500).send({ message: "Erro" + error });
  }
};

exports.confirmUserAdm = async (req, res) => {
  var user_id = req.jwtInfo._id;
  try {
    await userAdmRepository.confirmUser(user_id);
    res.sendFile(path.resolve("src/html" + "/confirm_user.html"));
  } catch (error) {
    return error;
  }
};

exports.resetPassAdm = async (req, res) => {
  var user_email = req.jwtInfo.user_email;
  try {
    var resetPass = await userAdmRepository.resetPass(user_email);
    res.status(201).send({ message: resetPass });
  } catch (error) {
    return error;
  }
};
