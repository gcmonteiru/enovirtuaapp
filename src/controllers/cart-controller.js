const cartRepository = require("../repositories/cart-repository");
const emptyMethod = require("../general-methods/is-empty");


exports.createCartItems = async (req, res) => {
  try {
    var data = req.body;
    var user_id = req.jwtInfo._id;

    var cartItemsAux = await cartRepository.listCartItems(user_id);
    var cartItems = cartItemsAux.user_cart[0];

    if (emptyMethod.isEmpty(cartItems)) {
      var cartItems = await cartRepository.createCartItems(data, user_id);
      return res.status(201).send({
        error: "false",
        message:
          "O item " + cartItems + ", foi adicionado ao carrinho com sucesso"
      });
    } else if (cartItems.establishment_id != data[0].establishment_id) {
      return res.status(400).send({
        error: "true",
        message:
          "Erro ao tentar adicionar item no carrinho, seu carrinho já possui itens de outro estabelecimento."
      });
    }
    var cartItems = await cartRepository.createCartItems(data, user_id);
    return res.status(201).send({
      error: "false",
      message:
        "O item " + cartItems + ", foi adicionado ao carrinho com sucesso"
    });
  } catch (error) {
    return error;
  }
};

exports.emptyCartItems = async (req, res) => {
  try {
    var user_id = req.jwtInfo._id;
    var cartItemsAux = await cartRepository.listCartItems(user_id);
    var cartItems = cartItemsAux.user_cart[0];

    if (emptyMethod.isEmpty(cartItems)) {
      return res.status(400).send({
        error: "true",
        message:
          "Erro ao tentar limpar o carrinho. O carrinho se encontra limpo."
      });
    }
    var cartItemsEmpty = await cartRepository.emptyCartItems(user_id);
    if (cartItemsEmpty == true) {
      return res.status(201).send({
        error: "false",
        message: "Carrinho limpo com sucesso."
      });
    }
  } catch (error) {
    return error;
  }
};

exports.listCartItems = async (req, res) => {
  var user_id = req.jwtInfo._id;
  try {
    var cart_list = await cartRepository.listCartItems(user_id);
    var cartItems = cart_list.user_cart[0];

    if (emptyMethod.isEmpty(cartItems)) {
      return res.status(400).send({
        error: "false",
        message: "Carrinho vazio."
      });
    }
    res.status(201).send({ cart_list });
  } catch (error) {
    return error;
  }
};
