const repository = require("../repositories/establishments-web-repository");
const userAdmRepository = require("../repositories/user-adm-repository")
const moment = require("moment");


exports.createEstablishments = async (req, res) => {
  try {

    var establishment_register_date = moment().subtract(3, "hours");
    var establishment_trial_valid = moment().add(90, "days");

    var createEstablishmentVar = await repository.createEstablishments({
      establishment_name: req.body.establishment_name,
      establishment_address: req.body.establishment_address,
      establishment_address2: req.body.establishment_address2,
      establishment_neighborhood: req.body.establishment_neighborhood,
      establishment_addressnumber: req.body.establishment_addressnumber,
      establishment_state: req.body.establishment_state,
      establishment_zipcode: req.body.establishment_zipcode,
      establishment_city: req.body.establishment_city,
      establishment_country: req.body.establishment_country,
      establishment_phone: req.body.establishment_phone,
      establishment_Xcoordinate: req.body.establishment_Xcoordinate,
      establishment_Ycoordinate: req.body.establishment_Ycoordinate,
      establishment_area_code: req.body.establishment_area_code,
      establishment_country_code: req.body.establishment_country_code,
      establishment_work_time: req.body.establishment_work_time,
      establishment_description: req.body.establishment_description,
      establishment_register: establishment_register_date,
      establishment_owner: req.jwtInfo._id,
      establishment_cnpj: req.body.establishment_cnpj,
      establishment_bank: req.body.establishment_bank,
      establishment_account_bank: req.body.establishment_account_bank,
      establishment_account_digit_bank:
        req.body.establishment_account_digit_bank,
      establishment_trial_valid: establishment_trial_valid,
      establishment_logo: req.body.establishment_logo
    });
    await userAdmRepository.setEstablishmentId(req.jwtInfo._id, createEstablishmentVar._id)
    return res
      .status(201)
      .send({ message: "Restaurante cadastrado com sucesso!" });
  } catch (e) {
    return res
      .status(500)
      .send({ message: "Falha ao cadastrar o restaurante." + " erro: " + e });
  }
};


exports.insertWineEstablishment = async (req, res) => {
  try {
    await repository.insertWineEstablishment(
      req.params.idEstablishment,
      req.params.idWine,
      req.params.priceWine
    );
    return res.status(201).send({ message: "Vinho inserido com sucesso!" });
  } catch (e) {
    return res
      .status(500)
      .send({ message: "Falha ao cadastrar o vinho." + " erro: " + e });
  }
};

exports.createImagesEstablishments = async (req, res) => {
  try {
    await repository.createImagesEstablishments(
      req.params.idEstablishment,
      req.body
    );
    return res.status(201).send({ message: "Imagens inseridas com sucesso!" });
  } catch (e) {
    return res
      .status(500)
      .send({ message: "Falha ao cadastrar as imagens." + " erro: " + e });
  }
};

exports.updateEstablishments = async (req, res) => {
  try {
    await repository.updateEstablishments(req.params.id, req.body);
    return res.status(200).send({
      message: "Restaurante atualizado com sucesso!"
    });
  } catch (e) {
    return res.status(500).send({
      message:
        "Falha ao atualizar a menção." +
        " " +
        e +
        " /n req.params.id:" +
        req.params.id +
        " /n reqbody:" +
        req.body
    });
  }
};

// delete
exports.deleteEstablishments = async (req, res) => {
  try {
    await repository.deleteEstablishments(req.params.id);
    res.status(200).send({
      message: "Restaurante removido com sucesso!"
    });
  } catch (e) {
    res.status(500).send({ message: "Falha ao remover o restaurante." });
  }
};

exports.listWinesofEstablishments = async (req, res) => {
  try {
    //await userRepository.updateCoordinates(req.params.id, req.body);
    const data = await repository.listwinesOfEstablishments(
      req.params.idEstablishment
    );
    res.status(200).send(data);
  } catch (e) {
    res
      .status(500)
      .send({ message: "Falha ao carregar os estabelecimentos!" + e });
  }
};


exports.findEstablishmentById = async (req, res) => {
  var establishment_id = req.params.idEstablishment;
  try {
    var establishmentInfo = await repository.findEstablishmentById(
      establishment_id
    );
    res.status(201).send({ establishmentInfo });
  } catch (error) {
    console.log(error);
  }
};



exports.updateWineOfEstablishment = async (req, res) => {
  var establishment_id = req.params.idEstablishment;
  var wine_id = req.params.idWine;
  var wine_new_price = req.params.priceWine;
  try {
    var wineUpdate = await repository.updateWineOfEstablishment(
      establishment_id, wine_id, wine_new_price
    );
    res.status(201).send({ error: "false", message: "wine alterado com sucesso" });
  } catch (error) {
    console.log(error);
  }
};
