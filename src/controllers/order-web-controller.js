const orderWebRepository = require("../repositories/order-web-repository");
const emptyMethod = require("../general-methods/is-empty");

exports.listPendingOrders = async (req, res) => {
  try {
    establishment_id = req.params.idestablishment;
    var pending_orders = await orderWebRepository.listPendingOrders(
      establishment_id
    );
    if (emptyMethod.isEmpty(pending_orders)) {
      return res.status(400).send({
        error: "false",
        message: "Nenhuma order pendente foi encontrada."
      });
    }
    res.status(201).send({ error: "false", pending_orders });
  } catch (error) {
    return error;
  }
};

exports.listConfirmedOrders = async (req, res) => {
  try {
    establishment_id = req.params.idestablishment;
    var confirmed_orders = await orderWebRepository.listConfirmedOrders(
      establishment_id
    );
    if (emptyMethod.isEmpty(confirmed_orders)) {
      return res.status(400).send({
        error: "false",
        message: "Nenhuma order confirmada foi encontrada."
      });
    }
    res.status(201).send({ error: "false", confirmed_orders });
  } catch (error) {
    return error;
  }
};

exports.listCanceledOrders = async (req, res) => {
  try {
    establishment_id = req.params.idestablishment;
    var canceled_orders = await orderWebRepository.listCanceledOrders(
      establishment_id
    );
    if (emptyMethod.isEmpty(canceled_orders)) {
      return res.status(400).send({
        error: "false",
        message: "Nenhuma order cancelada foi encontrada."
      });
    }
    res.status(201).send({ error: "false", canceled_orders });
  } catch (error) {
    return error;
  }
};

exports.confirmOrder = async (req, res) => {
  try {
    establishment_id = req.params.idestablishment;
    order = req.body.order_id;
    var listOrders = await orderWebRepository.confirmOrder(
      establishment_id,
      order
    );

    if (listOrders == "order_not_exists") {
      return res.status(400).send({
        error: "true",
        message: "Nenhuma order foi encontrada com esse id."
      });
    } else if (listOrders == "canceled_error") {
      return res.status(400).send({
        error: "true",
        message:
          "Não foi possível confirmar a order pois a mesma se encontrada cancelada."
      });
    } else if (listOrders == "already_confirmed") {
      return res.status(400).send({
        error: "true",
        message: "A order já está confirmada."
      });
    }

    var listOrdersbyId = await orderWebRepository.listOrdersById(
      listOrders.order_id
    );

    await orderWebRepository.registerOrderInHistoric(listOrdersbyId);
    res.status(201).send({ error: "false", message: "Order confirmada" });
  } catch (error) {
    return error;
  }
};

exports.cancelOrder = async (req, res) => {
  try {
    establishment_id = req.params.idestablishment;
    order = req.body.order_id;
    canceled_reason = req.body.canceled_reason;
    if (emptyMethod.isEmpty(canceled_reason)) {
      return res.status(400).send({
        error: "true",
        message: "É necessário escolher um motivo para cancelar a order."
      });
    }
    var listOrders = await orderWebRepository.cancelOrder(
      establishment_id,
      order,
      canceled_reason
    );

    if (listOrders == "order_not_exists") {
      return res.status(400).send({
        error: "true",
        message: "Nenhuma order foi encontrada com esse id."
      });
    } else if (listOrders == "already_canceled") {
      return res.status(400).send({
        error: "true",
        message:
          "Não foi possível cancelar a order pois a mesma se encontrada cancelada."
      });
    }
    res.status(201).send({ error: "false", message: "Order cancelada" });
  } catch (error) {
    return error;
  }
};
