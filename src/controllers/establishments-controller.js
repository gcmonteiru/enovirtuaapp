const repository = require("../repositories/establishments-repository");
const emptyMethod = require("../general-methods/is-empty");

exports.listEstablishments = async (req, res) => {
  try {
    var limit = req.params.limit;
    var skip = req.params.skip;
    const data = await repository.listEstablishments(req.jwtInfo._id, req.body, limit,skip );
    res.status(200).send(data);
  } catch (e) {
    res
      .status(500)
      .send({ message: "Falha ao carregar os estabelecimentos!" + e });
  }
};

exports.searchEstablishment = async (req, res) => {
  try {
    var limit = req.params.limit;
    var skip = req.params.skip;
    var user_id = req.jwtInfo._id
    var establishment = req.body.establishment_search;
    if (emptyMethod.isEmpty(establishment)) {
      return res.status(400).send({
        error: "true",
        message: "Erro ao procurar estabelecimentos, campo de busca vazio."
      });
    }

    const data = await repository.searchEstablishments(establishment, user_id, limit,skip);

    if (emptyMethod.isEmpty(data)) {
      return res.status(400).send({
        error: "true",
        message: "Nenhum estabelecimento foi encontrado."
      });
    }
    res.status(200).send(data);
  } catch (e) {
    res
      .status(500)
      .send({ message: "Falha ao carregar os estabelecimentos!" + e });
  }
};


exports.listWinesofEstablishments = async (req, res) => {
  try {
    //await userRepository.updateCoordinates(req.params.id, req.body);
    const data = await repository.listwinesOfEstablishments(
      req.params.idEstablishment
    );
    res.status(200).send(data);
  } catch (e) {
    res
      .status(500)
      .send({ message: "Falha ao carregar os vinhos do estabelecimento!" + e });
  }
};


exports.listMenuOfEstablishments = async (req, res) => {
  try {
    //await userRepository.updateCoordinates(req.params.id, req.body);
    const data = await repository.listMenuOfEstablishments(
      req.params.idEstablishment
    );
    res.status(200).send(data);
  } catch (e) {
    res
      .status(500)
      .send({ message: "Falha ao carregar o menu do estabelecimento!" + e });
  }
};


exports.findEstablishmentById = async (req, res) => {
  var establishment_id = req.establishmentid._id;
  try {
    var establishmentInfo = await repository.findEstablishmentById(
      establishment_id
    );
    res.status(201).send({ establishmentInfo });
  } catch (error) {
    console.log(error);
  }
};


exports.register = async (req, res) => {
  try {
    var establishmentInfo = await repository.register(
    );
    res.status(201).send({ done: "done" });
  } catch (error) {
    console.log(error);
  }
};