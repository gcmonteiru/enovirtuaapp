const repository = require("../repositories/wines-web-repository");
const emptyMethod = require("../general-methods/is-empty");

exports.listWines = async (req, res) => {
  try {
    var limit = req.params.limit;
    var skip = req.params.skip;
    const data = await repository.listWines(limit, skip);
    res.status(200).send(data);
  } catch (e) {
    res.status(500).send({ message: "Foi encontrado um erro ao listar os vinhos", error: e });
  }
};

exports.updateWines = async (req, res) => {

  try {
    await repository.updateWines(req.params.id, req.body);
    return res.status(200).send({
      message: "Vinho atualizado com sucesso!"
    });
  } catch (e) {
    return res
      .status(500)
      .send({
        message:
          "Falha ao atualizar o vinho." +
          " " +
          e +
          " /n req.params.id:" +
          req.params.id +
          " /n reqbody:" +
          req.body
      });
  }
};

exports.searchWines = async (req, res) => {
  try {
    var skip = req.params.skip
    var limit = req.params.limit
    var wine = req.body.wine_search;
    if (emptyMethod.isEmpty(wine)) {
      return res.status(400).send({
        error: "true",
        message: "Erro ao procurar vinhos, campo de busca vazio."
      });
    }
    const data = await repository.searchWines(wine, skip, limit);

    if (emptyMethod.isEmpty(data)) {
      return res.status(400).send({
        error: "true",
        message: "Nenhum estabelecimento foi encontrado."
      });
    }
    res.status(200).send(data);
  } catch (e) {
    res
      .status(500)
      .send({ message: "Falha ao carregar os estabelecimentos!" + e });
  }
};

// delete
exports.deleteWines = async (req, res) => {
  try {
    await repository.deleteWines(req.params.id);
    res.status(200).send({
      message: "Menção removida com sucesso!"
    });
  } catch (e) {
    res.status(500).send({ message: "Falha ao remover o vinho." });
  }
};

exports.registerWines = async (req, res) => {
  try {
    const data = await repository.winesRegister(req.body);
    res.status(200).send({ message: "Vinhos gravados com sucesso." + data });
  } catch (e) {
    res
      .status(500)
      .send({ message: "Falha ao registrar os vinhos!" + e });
  }
};
