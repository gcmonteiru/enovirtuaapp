const orderRepository = require("../repositories/order-repository");
const userRepository = require("../repositories/user-repository");
const emptyMethod = require("../general-methods/is-empty");

exports.createOrder = async (req, res) => {
  try {
    user_id = req.jwtInfo._id;
    user_card_id = req.body.card_id;
    card_cvv = req.body.card_cvv;
    booking_observations = req.body.booking_observations;
    booking_wine_open = req.body.booking_wine_open;
    booking_payment = req.body.booking_payment;
    booking_time = req.body.booking_time;

    var createOrder = await orderRepository.createOrder(
      user_id,
      user_card_id,
      card_cvv,
      booking_observations,
      booking_wine_open,
      booking_payment,
      booking_time
    );

    if (createOrder == "empty_cart") {
      return res.status(400).send({
        error: true,
        message: "Erro ao processar compra, carrinho de compras vazio."
      });
    } else if (createOrder == "invalid_country") {
      return res.status(400).send({
        error: true,
        message:
          "Erro ao processar compra, País invalido. País validos para compra: Brasil."
      });
    } else if (createOrder == "invalid_card") {
      return res.status(400).send({
        error: true,
        message: "Erro ao processar compra, o cartão selecionado não existe."
      });
    } else if (createOrder == "order_need_payment") {
      return res.status(400).send({
        error: true,
        message:
          "Erro ao processar compra, o pagamento precisa ser realizado antecipadamente para abrir o vinho."
      });
    }
    res.status(201).send({ createOrder });
  } catch (error) {
    return error;
  }
};

exports.listOrders = async (req, res) => {
  try {
    user_id = req.jwtInfo._id;
    var listOrders = await userRepository.getUserOrdersHistoric(user_id);
    res.status(201).send(listOrders);
  } catch (error) {
    return error;
  }
};

exports.cancelOrder = async (req, res) => {
  try {
    establishment_id = req.params.idestablishment;
    order = req.body.order_id;
    canceled_reason = req.body.canceled_reason;

    if (emptyMethod.isEmpty(canceled_reason)) {
      return res.status(400).send({
        error: "true",
        message: "É necessário escolher um motivo para cancelar a order."
      });
    }

    var listOrders = await orderRepository.cancelOrder(
      establishment_id,
      order,
      canceled_reason
    );

    if (listOrders == "order_not_exists") {
      return res.status(400).send({
        error: "true",
        message: "Nenhuma order foi encontrada com esse id."
      });
    } else if (listOrders == "already_canceled") {
      return res.status(400).send({
        error: "true",
        message:
          "Não foi possível cancelar a order pois a mesma se encontrada cancelada."
      });
    }
    res.status(201).send({ error: "false", message: "Order cancelada" });
  } catch (error) {
    return error;
  }
};
