const express = require("express");
const jwt = require("jsonwebtoken");
const bodyParser = require("body-parser");
const mongodb = require("../src/mongodb/connect")
const cors = require("cors");
const jwt_info_checker = require("./general-methods/jwt-info-checker");
require("dotenv").config();


const winesRoutes = require("./routes/wines-routes");
const winesWebRoutes = require("./routes/wines-web-routes");
const userRoutes = require("./routes/user-routes");
const establishmentsRoutes = require("./routes/establishments-routes");
const establishmentsWebRoutes = require("./routes/establishments-web-routes");
const cartRoutes = require("./routes/cart-routes");
const orderRoutes = require("./routes/order-routes");
const ebanxRoutes = require("./routes/ebanx-routes");
const userWebFrontRoutes = require("./routes/user-adm-routes");
const orderWebRoutes = require("./routes/order-web-routes");

const app = express();

app.use(bodyParser.json({ limit: "40mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "40mb", extended: true, parameterLimit: 1000000}));

app.use(
  cors({
    origin: "https://enovirtua.herokuapp.com//"
  })
);
app.use(
  "/api/v1.0/establishments",
  function(req, res, next) {
    try {
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_API)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_API);
        var isJwtInfoCorrect = jwt_info_checker.verifyUserIdEmail(req.jwtInfo);
        if (isJwtInfoCorrect.error == "true") {
          res.status(400).send(isJwtInfoCorrect);
          return;
        }
        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  establishmentsRoutes
);

app.use(
  "/api/v1.0/establishments-web",
  function(req, res, next) {
    try {
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_API)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_API);
        var isJwtInfoCorrect = jwt_info_checker.verifyUserIdEmail(req.jwtInfo);
        if (isJwtInfoCorrect.error == "true") {
          res.status(400).send(isJwtInfoCorrect);
          return;
        }

        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  establishmentsWebRoutes
);

app.use(
  "/api/v1.0/wines",
  function(req, res, next) {
    try {
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_API)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_API);
        var isJwtInfoCorrect = jwt_info_checker.verifyUserIdEmail(req.jwtInfo);
        if (isJwtInfoCorrect.error == "true") {
          res.status(400).send(isJwtInfoCorrect);
          return;
        }
        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  winesRoutes
);

app.use(
  "/api/v1.0/wines-web",
  function(req, res, next) {
    try {
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_API)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_API);
        var isJwtInfoCorrect = jwt_info_checker.verifyUserIdEmail(req.jwtInfo);
        if (isJwtInfoCorrect.error == "true") {
          res.status(400).send(isJwtInfoCorrect);
          return;
        }
        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  winesWebRoutes
);

app.use("/api/v1.0/user", userRoutes);

app.use("/api/v1.0/web-enovirtua", userWebFrontRoutes);

app.use(
  "/api/v1.0/order-web",
  function(req, res, next) {
    try {
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_API)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_API);
        var isJwtInfoCorrect = jwt_info_checker.verifyUserIdEmail(req.jwtInfo);
        if (isJwtInfoCorrect.error == "true") {
          res.status(400).send(isJwtInfoCorrect);
          return;
        }
      }
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  orderWebRoutes
);

app.use("/ebanx", ebanxRoutes);

app.use(
  "/api/v1.0/cart",
  function(req, res, next) {
    try {
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_API)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_API);
        var isJwtInfoCorrect = jwt_info_checker.verifyUserIdEmail(req.jwtInfo);
        if (isJwtInfoCorrect.error == "true") {
          res.status(400).send(isJwtInfoCorrect);
          return;
        }
        next();
      }
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  cartRoutes
);

app.use(
  "/api/v1.0/order",
  function(req, res, next) {
    try {
      const jwtString = req.get("Authorization");
      if (jwt.verify(jwtString, process.env.SECRET_API)) {
        req.jwtInfo = jwt.decode(jwtString, process.env.SECRET_API);
        var isJwtInfoCorrect = jwt_info_checker.verifyUserIdEmail(req.jwtInfo);
        if (isJwtInfoCorrect.error == "true") {
          res.status(400).send(isJwtInfoCorrect);
          return;
        }
      }
      next();
    } catch (error) {
      res.status(401).send({ message: "Erro de autenticação: " + error });
    }
  },
  orderRoutes
);

module.exports = app;
