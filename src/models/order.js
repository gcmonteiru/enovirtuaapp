var mongoose = require("mongoose");
var orderSchema = new mongoose.Schema({
  order_items: {
    type: Array,
    lowercase: true,
    required: true
  },
  order_price: {
    type: String,
    lowercase: true,
    required: true
  },
  order_owner: {
    type: String,
    lowercase: true,
    required: true
  },
  order_establishment: {
    type: String,
    lowercase: true,
    required: true
  },
  order_status_establishment: {
    type: String,
    required: true,
    default: "pending"
  },
  order_status_payment: {
    type: String,
    lowercase: true,
    required: true
  },
  order_status_establishment: {
    type: String,
    lowercase: true,
    default: "pending"
  },
  order_id: {
    type: String,
    lowercase: true,
    required: true
  },
  order_has_payment: {
    type: String,
    lowercase: true,
    required: true
  },
  order_payment_type: {
    type: String,
    lowercase: true,
    required: true
  },
  order_payment_info: {
    type: String,
    lowercase: true,
    required: true
  },
  order_time: {
    type: Date,
    lowercase: true,
    required: true
  },
  order_booking_time: {
    type: Date,
    lowercase: true,
    required: true
  },
  order_booking_open: {
    type: String,
    lowercase: true,
    required: true
  },
  order_observations: {
    type: String,
    lowercase: true,
    required: false
  },
  order_canceled_reason: {
    type: String,
    default: "",
    required: false
  },
  order_canceled_by: {
    type: String,
    default: "",
    required: false
  },
  order_owner_name: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model("order", orderSchema);
