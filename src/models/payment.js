var mongoose = require('mongoose');
var paymentSchema = new mongoose.Schema({
  user_email: {
    type: String,
    lowercase: true,
    required: true}
});

module.exports = mongoose.model('payment', paymentSchema);