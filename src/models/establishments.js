const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema({
  establishment_name: {
    type: String,
    required: true,
    trim: true
  },
  establishment_address: {
    type: String,
    required: true
  },
  establishment_address2: {
    type: String,
    required: false,
    default: ""
  },
  establishment_neighborhood: {
    type: String,
    required: true
  },
  establishment_addressnumber: {
    type: String,
    required: true
  },
  establishment_state: {
    type: String,
    required: true
  },
  establishment_zipcode: {
    type: String,
    required: true
  },
  establishment_city: {
    type: String,
    required: true
  },
  establishment_country: {
    type: String,
    required: true
  },
  establishment_phone: {
    type: String,
    required: true
  },
  establishment_area_code: {
    type: String,
    required: true
  },
  establishment_country_code: {
    type: String,
    required: true
  },
  establishment_Xcoordinate: {
    type: String,
    required: true
  },
  establishment_Ycoordinate: {
    type: String,
    required: true
  },
  establishment_listMenu: { type: Array, required: false },
  establishment_listWines: { type: Array, required: false },
  establishment_subscription_history: { type: Array, required: false },
  establishment_distanceFromUser: {
    type: Number,
    required: false,
    default: 0
  },
  establishment_register: {
    type: Date,
    required: true
  },
  establishment_trial_valid: {
    type: Date,
    required: true
  },
  establishment_owner: {
    type: String,
    required: true
  },
  establishment_actived: {
    type: Number,
    required: true,
    default: 1
  },
  establishment_cnpj: {
    type: String,
    required: false
  },
  establishment_bank: {
    type: String,
    required: false
  },
  establishment_account_bank: {
    type: String,
    required: false
  },
  establishment_account_digit_bank: {
    type: String,
    required: false
  },
  establishment_work_time: {
    type: String,
    required: true
  },
  establishment_description: {
    type: String,
    required: false
  },
  establishment_logo: {
    type: String,
    required: false,
    default: "https://image.flaticon.com/icons/svg/45/45332.svg"
  },
  establishment_orders: { type: Array, required: false },
  establishment_orders_historic: { type: Array, required: false },
  establishment_images: {
    type: Array,
    required: false,
    default:
      "https://previews.123rf.com/images/keltt/keltt1509/keltt150900004/44670540-restaurant-icon-with-grapes-and-wine-glass-silhouette.jpg"
  }
});

module.exports = mongoose.model("Establishments", schema);
