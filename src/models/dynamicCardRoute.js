var mongoose = require('mongoose');
var dynamicCardRouteSchema = new mongoose.Schema({
  route_id: {
    type: String,
    lowercase: true,
    required: false}
});

module.exports = mongoose.model('dynamicCardRoute', dynamicCardRouteSchema);