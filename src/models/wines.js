const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  wine_name: {
    type: String,
    required: false,
    trim: true
  },
  wine_region: {
    type: String,
    required: false
  },
  wine_country: {
    type: String,
    required: false
  },
  wine_winery: {
    type: String,
    required: false
  },
  wine_harvest: {
    type: String,
    required: false
  },
  wine_grape: {
    type: String,
    required: false
  },
  wine_type: {
    type: String,
    required: false
  },
  wine_varietal: {
    type: String,
    required: false
  },
  wine_description: {
    type: String,
    required: false
  },
  wine_classification: {
    type: String,
    required: false
  },
  wine_viticultura: {
    type: String,
    required: false
  },
  wine_terroir: {
    type: String,
    required: false
  },
  wine_harmonization: {
    type: String,
    required: false
  },
  wine_consumption: {
    type: String,
    required: false
  },
  wine_tenor: {
    type: String,
    required: false
  },
  wine_volume: {
    type: String,
    required: false
  },
  wine_acidity: {
    type: String,
    required: false
  },
  wine_ph: {
    type: String,
    required: false
  },
  wine_extract: {
    type: String,
    required: false
  },
  wine_vinification: {
    type: String,
    required: false
  },
  wine_aroma: {
    type: String,
    required: false
  },
  wine_color: {
    type: String,
    required: false
  },
  wine_guarda: {
    type: String,
    required: false
  },
  wine_rewards: {
    type: String,
    required: false
  },
  wine_label: {
    type: String,
    required: false
  },
  wine_barcode: {
    type: String,
    required: false
  },
  wine_price: {
    type: String,
    required: false
  },
  wine_degustation: {
    type: String,
    required: false
  },
  wine_register: {
    type: String,
    required: false
  },
  wine_autor: {
    type: String,
    required: false
  },
  wine_status: {
    type: String,
    required: false
  },


  imagesWines:{ type: Array, required: false, default: "https://www.wine.com.br/cdn-cgi/image/f=auto,h=580,q=100/assets-images/produtos/23193-01.png"}
});

module.exports = mongoose.model('Wines', schema);