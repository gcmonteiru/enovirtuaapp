var mongoose = require("mongoose");
var userSchema = new mongoose.Schema({
  user_email: {
    type: String,
    lowercase: true,
    required: true,
    unique:true
  },
  user_pass: {
    type: String,
    required: true
  },
  user_name: {
    type: String,
    lowercase: true,
    required: true
  },
  user_born: {
    type: Date,
    required: true
  },
  user_Xcoordinate: {
    type: String,
    require: false,
    default: ""
  },
  user_Ycoordinate: {
    type: String,
    require: false,
    default: ""
  },
  user_desiredDistanceInKm: {
    type: String,
    require: false,
    default: 10
  },
  user_confirmed: {
    type: Number,
    require: false,
    default: 0 // not confirmed
  },
  user_phone: {
    type: String,
    require: true
  },
  user_cart: { type: Array, required: false },
  user_credit_card: {
    type: Array, required: false},
  user_orders_historic: { type: Array, require: false },
  user_request_help: { type: Array, require: false },
  user_address: {
    type: String,
    require: true
  },
  user_address2: {
    type: String,
    required: false,
    default: ""
  },
  user_neighborhood: {
    type: String,
    required: true
  },
  user_addressnumber: {
    type: String,
    required: true
  },
  user_state: {
    type: String,
    required: true
  },
  user_adm: {
    type: String,
    required: false,
    default: "false"
  },
  user_zipcode: {
    type: String,
    required: true
  },
  user_city: {
    type: String,
    required: true
  },
  user_country: {
    type: String,
    required: true
  },
  user_phone: {
    type: String,
    required: true
  },
  user_registered: {
    type: Date,
    required: true
  },
  user_cpf: {
    type: String,
    required: true
  },
  user_cart_price: {
    type: String,
    required: false,
    default: ""
  },
  user_country_code: {
    type: String,
    required: true,
    default: ""
  },
  user_area_code: {
    type: String,
    required: true,
    default: ""
  }
});

userSchema
  .path('user_cpf')
  .validate(function(cpf) {
    var i = 0; // index de iteracao
    var somatoria = 0;
    var cpf = cpf.toString().split("");
    var dv11 = cpf[cpf.length - 2]; // mais significativo
    var dv12 = cpf[cpf.length - 1]; // menos significativo
    cpf.splice(cpf.length - 2, 2); // remove os digitos verificadores originais
    for(i = 0; i < cpf.length; i++) {
      somatoria += cpf[i] * (10 - i);
    }
    var dv21 = (somatoria % 11 < 2) ? 0 : (11 - (somatoria % 11));
    cpf.push(dv21);
    somatoria = 0;
    for(i = 0; i < cpf.length; i++) {
      somatoria += cpf[i] * (11 - i);
    }
    var dv22 = (somatoria % 11 < 2) ? 0 : (11 - (somatoria % 11));

    if (dv11 == dv21 && dv12 == dv22) {
      return true
    } else {
      return false
    }
  }, '{PATH} validation failed.');

module.exports = mongoose.model("user", userSchema);
