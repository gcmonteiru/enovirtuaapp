var mongoose = require("mongoose");

var ebanxSchema = new mongoose.Schema({
  payment: {type: Object,
    lowercase: true,
    required: false,
  },
  status: {
    type: String,
    lowercase: true,
    required: false
  }
});

module.exports = mongoose.model("ebanx", ebanxSchema);
