const empty = require("is-empty");

exports.isEmpty = data => {
  try {
    if (empty(data)) {
      return true;
    }
    return false;
  } catch (error) {
    return error;
  }
};
