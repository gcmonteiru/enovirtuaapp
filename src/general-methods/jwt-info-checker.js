const empty = require("is-empty");

exports.verifyUserIdEmail = (data) => {
    try {
      if (empty(data._id) || empty(data.user_email)) {
        return (response = {
          error: "true",
          message:
            "O jwt token não possui as informações necessárias para trabalhar nessa rota. Informações necessárias: user_id, user_email."
        });
      }
      return (response = {
        error: "false",
        message: "ok"
      });
    } catch (error) {
      res.status(500).send({ message: "Erro" + error });
    }
  }

  
exports.verifyUserEmailPass = (data) => {
    try {
      if (empty(data.user_pass) || empty(data.user_email)) {
        return (response = {
          error: "true",
          message:
            "O jwt token não possui as informações necessárias para trabalhar nessa rota. Informações necessárias: user_email, user_pass."
        });
      }
      return (response = {
        error: "false",
        message: "ok"
      });
    } catch (error) {
      res.status(500).send({ message: "Erro" + error });
    }
  }