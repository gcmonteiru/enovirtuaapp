const emptyMethod = require("../general-methods/is-empty");
const establishmentRepository = require("../repositories/establishments-repository");
const moment = require("moment");

/* CART DATA CHECKER */

exports.cartDataEmptyChecker = data => {
  try {
    jsonStr = JSON.parse(JSON.stringify(data[0]));
    if (
      emptyMethod.isEmpty(jsonStr["item_id"]) ||
      emptyMethod.isEmpty(jsonStr["item_price"]) ||
      emptyMethod.isEmpty(jsonStr["establishment_id"]) ||
      emptyMethod.isEmpty(jsonStr["item_image"]) ||
      emptyMethod.isEmpty(jsonStr["item_name"])
    ) {
      return true;
    }

    return false;
  } catch (error) {
    return error;
  }
};

exports.cartDataValidChecker = async data => {
  try {
    jsonStr = JSON.parse(JSON.stringify(data[0]));

    var establishmentId = await establishmentRepository.findEstablishmentById(
      jsonStr["establishment_id"]
    );

    if (emptyMethod.isEmpty(establishmentId)) {
      return true;
    }

    return false;
  } catch (error) {
    return error;
  }
};

exports.orderDataEmptyChecker = data => {
  try {
    jsonStr = JSON.parse(JSON.stringify(data));
    if (
      emptyMethod.isEmpty(jsonStr["card_cvv"]) ||
      emptyMethod.isEmpty(jsonStr["booking_payment"]) ||
      emptyMethod.isEmpty(jsonStr["booking_wine_open"]) ||
      emptyMethod.isEmpty(jsonStr["booking_time"]) ||
      emptyMethod.isEmpty(jsonStr["card_id"])
    ) {
  
      return true;
    }
    return false;
  } catch (error) {
    return error;
  }
};

exports.orderDataValidChecker = data => {
  try {
    jsonStr = JSON.parse(JSON.stringify(data));

    if (
      String(jsonStr["card_cvv"]).length != 3 ||
      !String(jsonStr["booking_time"]).match(
        /(19|20)[0-9][0-9]-(0[0-9]|1[0-2])-(0[1-9]|([12][0-9]|3[01]))T([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]/
      )
    ) {
      return true;
    }
    return false;
  } catch (error) {
    return error;
  }
};

exports.orderDateValidChecker = data => {
  try {
    jsonStr = JSON.parse(JSON.stringify(data));
    var dateToSplit = String(jsonStr["booking_time"]).split("T");

    var dateMonthDayYear = dateToSplit[0].split("-");
    var dateMonthDayYear2 =
      dateMonthDayYear[0] +
      "/" +
      dateMonthDayYear[1] +
      "/" +
      dateMonthDayYear[2];

    var dateHourMinute = dateToSplit[1].split(".");

    var dateHourMinute2 = dateHourMinute[0].split(":");
console.log( dateMonthDayYear2 +
  " " +
  dateHourMinute2[0] +
  ":" +
  dateHourMinute2[1] +
  ":00Z")
    var m = moment.utc( dateMonthDayYear2 +
      " " +
      dateHourMinute2[0] +
      ":" +
      dateHourMinute2[1] +
      ":00Z", "YYYY-MM-DD  HH:mm:ss");
 
    var timeNow = moment().subtract(1, "hours");
    console.log(m)
    var isafter = m.isAfter(timeNow);
    if (isafter) {
      return false;
    }
    return true;
  } catch (error) {
    return error;
  }
};

exports.orderDataLogicChecker = data => {
  try {
    jsonStr = JSON.parse(JSON.stringify(data));

    if (
      jsonStr["booking_payment"] == "false" &&
      jsonStr["booking_wine_open"] == "true"
    ) {
      return true;
    }
    return false;
  } catch (error) {
    return error;
  }
};

exports.wineDataEmptyChecker = data => {
  try {
    jsonStr = JSON.parse(JSON.stringify(data));
    if (
      emptyMethod.isEmpty(jsonStr["idEstablishment"]) ||
      emptyMethod.isEmpty(jsonStr["idWine"]) ||
      emptyMethod.isEmpty(jsonStr["priceWine"])
    ) {
  
      return true;
    }
    return false;
  } catch (error) {
    return error;
  }
};